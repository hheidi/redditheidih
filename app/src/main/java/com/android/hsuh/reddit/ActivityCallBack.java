package com.android.hsuh.reddit;

import android.net.Uri;

public interface ActivityCallBack {
    void onPostSelected(Uri redditPostUri);
}
