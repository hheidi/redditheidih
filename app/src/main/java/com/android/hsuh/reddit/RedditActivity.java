package com.android.hsuh.reddit;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;


public class RedditActivity extends SingleFragmentActivity implements ActivityCallBack{

    private MediaPlayer mediaPlayer;


    @Override
    protected Fragment createFragment() {
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sound);

        return new RedditListFragment();
    }

    @Override
    public void onPostSelected(Uri redditPostUri) {
        mediaPlayer.start();
        Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
        intent.setData(redditPostUri);
        startActivity(intent);
    }


}
