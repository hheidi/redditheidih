package com.android.hsuh.reddit;

import android.media.MediaPlayer;

import java.util.StringTokenizer;

public class RedditPost {

    public String title;
    public String url;

    public RedditPost(String title, String url) {
        this.title = title;
        this.url = url;
    }
}